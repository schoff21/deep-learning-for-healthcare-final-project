# Deep Learning For Healthcare Final Project

Original Paper : https://www-scopus-com.proxy2.library.illinois.edu/record/display.uri?eid=2-s2.0-85088616023&origin=inward&txGid=08ffe5e5c6291151eaf89a5f6aaccf93&featureToggles=FEATURE_NEW_DOC_DETAILS_EXPORT:1

## Dependencies

- PyTorch
- Pandas
- Numpy
- gensim
- torchcrf

## Downloading Dataset

The dataset used for project was used for the 2010 i2b2/VA Natural Language Processing Challenge. You can request access to this dataset here https://portal.dbmi.hms.harvard.edu/projects/n2c2-nlp/.

## Downloading the GloVe Word Embedding 

This project uses the 2014 6B token GloVe model for word embedding. This can be downloaded at https://nlp.stanford.edu/projects/glove

## Preprocessing

All preprocessing is done in the file [DataFrameCreator.ipynb](DataFrameCreator.ipynb). Open this notebook and follow the instructions to perform the data preprocessing.

## Training and Evaluation

Training and evaluation is done in the file [MedicalConceptLSTM.ipynb](MedicalConceptLSTM.ipynb). Open this notebook and follow the instructions to perform training and evaluation.

## Pretrained Models

Each of the models I have trained are availble in this repository. To run the pretrained models, scroll to the evaluation portion of [MedicalConceptLSTM.ipynb](MedicalConceptLSTM.ipynb) and set `trainNewModel = False`. Here you can uncomment whichever pretrained model you want to test.

## Results

Structure | Precision | Recall | F1-score
--- | --- | --- | --- 
uniLSTM | 80.46% | 77.52% | 78.88%
biLSTM | 85.67% | 81.59% | 83.53%
Attn LSTM | 85.37% | 81.57% | 83.34%
4-layer LSTM | 86.07% | 82.34% | 84.13%
FC LSTM | 85.34% | 82.93% | 84.11%
